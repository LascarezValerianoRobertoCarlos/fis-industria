Industria 4.0 Mapa conceptual
```plantuml
@startmindmap

*[#LightGoldenRodYellow]  **Industria 4.0**    
 *_ Sus
  *[#LightGreen]  **Antecedentes** 
   *[#LightPink]  **Industria 1.0 (1784)**
    *_ Se basa en
     *[#LightBlue]  **Producciòn Mecanica**
    *_ Equipos basados en
     *[#LightBlue]  **Energìa**
      *[#LightBlue]  **Hidràulica**
      *[#LightBlue]  **Vapor**
   *[#LightPink]  **Industria 2.0 (1870)**
    *_ Linea de
     *[#LightBlue]  **Ensamblaje**
    *_ Se basa en
     *[#LightBlue]  **Energìa Elèctrica**
    *_ Se llevo acabo la 
     *[#LightBlue]  **Producciòn en masa**
      *_ Ejemplo
       *[#LightBlue]  **Producciòn de carros**
   *[#LightPink]  **Industria 3.0 (1969)**
    *_ se basan en
     *[#LightBlue]  **Producciòn** \n **automatizada**
      *_ Usando
       *[#LightBlue]  **Electrònica**
 *[#LightGreen]  **Producciòn inteligente con** \n **decisiòn autònoma usando** \n **tecnologìa como IOT,** \n **IA, Big Data, Cloud, Robot, etc**
  *[#LightBlue]  **Cambios**
   *[#LightPink]  **Integraciòn** \n **de TICS**
    *_ en la 
     *[#LightBlue]  **Industria**
      *_ de
       *[#LightBlue]  **Manufactura**
      *_ y
       *[#LightBlue]  **Servicios**
    *[#LightBlue]  **Consecuencias**
     *_ Apariciòn de
      *[#LightBlue]  **Nuevos empleos**
     *_ Reducciòn de 
      *[#LightBlue]  **Puestos de** \n ** trabajo**
       *_ En estas 
        *[#LightBlue]  **Àreas**
         *[#LightBlue]  **Producciòn**
         *[#LightBlue]  **Manejo de** \n **inventarios**
         *[#LightBlue]  **Entrega de** \n **productos**
   *[#LightPink]  **Empresas de** \n **manufactura**
    *_ en
     *[#LightBlue]  **Empresas** \n **de TICS**
      *_ se originaron
       *[#LightBlue]  **Modernos**
        *[#LightBlue]  **Electrodomèsticos**
         *_ Cuentan con
          *[#LightBlue]  **Sistema Operativo**
          *[#LightBlue]  **Procesamiento de** \n **lenguaje natural**
          *[#LightBlue]  **Comunicaciòn inalàmbrica**
          *[#LightBlue]  **Software**
          *[#LightBlue]  **Computaciòn en** \n **la nube**
      *_ Existen menos
       *[#LightBlue]  **Diferenciaciòn**
        *_ Entre las
         *[#LightBlue]  **Empresas**
   *_ Nuevos
    *[#LightPink]  **Paradigmas y** \n **tecnologìas**
     *[#LightBlue]  **Negocios**
      *_ Basados en
       *[#LightBlue]  **Plataformas**
        *_ las Empresas
         *[#LightBlue]  **Lìderes**
          *[#LightBlue]  **FANG**
          *[#LightBlue]  **BAT**
     *[#LightBlue]  **Big Data e** \n **Inteligencia Artificial**
      *_ Se basa en el 
       *[#LightBlue]  **Manejo** \n **de datos**
   *_ Nuevas
    *[#LightPink]  **Culturas Digitales**
     *[#LightBlue]  **Generaciòn**
      *[#LightBlue]  **Phono Sapiens**
       *_ Nacidos en \n la era 
        *[#LightBlue]  **Digital**
         *_ Ejemplos
          *[#LightBlue]  **YouTubers**
          *[#LightBlue]  **E-Sports**
@endmindmap
```mindmap
```
Industria 4.0 en Mèxico-Mapa conceptual
```plantuml
@startmindmap
*[#LightGoldenRodYellow]  **Industria 4.0** \n **en Mèxico** 
 *[#LightGreen]  **Introducciòn de las** \n **tecnologìas digitales** \n **en las fabricas**
  *_ Con sus
   *[#LightPink]  **Beneficios e inconvenientes**
  *_ Mèxico se \n encuentra en
   *[#LightPink]  **Punto de inflexiòn**
    *_ falta de
     *[#LightBlue]  **Conocimiento**
    *_ ante la 
     *[#LightBlue]  **Revoluciòn Industrial**
    *_ bloque \n ante el  
     *[#LightBlue]  **Miedo**
  *_ Ejemplo de
   *[#LightPink]  **Empresas**
    *[#LightBlue]  **STARTUPS**
     *[#LightBlue]  **Aplicaciòn** \n **de seguridad**
      *_ De los 
       *[#LightBlue]  **Hogares**
    *[#LightBlue]  **WEAROBOT**
     *_ Crean
      *[#LightBlue]  **Exoesqueletos**
  *[#LightPink]  **Internet de** \n **las cosas**
   *[#LightBlue]  **Tecnologìa**
    *_ Que puede
     *[#LightBlue]  **Adaptarse**
      *_ a una infinidad de
       *[#LightBlue]  **Procesos Industriales** 
    
          
     
@endmindmap
```mindmap
```